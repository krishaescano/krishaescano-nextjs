import {Navbar, Nav} from 'react-bootstrap';

export default function NavBar (){
	return (
	<Navbar fixed="top" bg="light" expand="lg" id="mainNav">
		<Navbar.Brand href="#home">
			<img className="logo float-left" src="./krishalogo.png" alt="mylogo"/>
		</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
		<Nav className="ml-auto my-lg-0">
			<Nav.Link className="navLink" href="home">Home</Nav.Link>
			<Nav.Link className="navLink" href="about">About</Nav.Link>
			<Nav.Link className="navLink" href="/project">Project</Nav.Link>
			<Nav.Link className="navLink" href="/contact">Contact</Nav.Link>
			<Nav.Link className="navLink" href="日本語">日本語</Nav.Link>
		</Nav>
	</Navbar.Collapse>
</Navbar>
	)
}