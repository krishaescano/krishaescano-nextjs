import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import Particles from 'react-particles-js';
import NavBar from '../components/NavBar';
import {Link} from 'react-scroll';

function MyApp({ Component, pageProps }) {
  return (
	  	<Component {...pageProps} />
  )
}

export default MyApp
